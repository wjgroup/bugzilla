﻿using System;
using System.Collections.Generic;
using System.Linq;

using Xamarin.Forms;

namespace NavTests
{
    public partial class Page4 : ContentPage
    {
        public Page4()
        {
            InitializeComponent();

            Title = "Page 4";
        }

        async void Handle_Clicked(object sender, System.EventArgs e)
        {
            Navigation.RemovePage(Navigation.NavigationStack.ElementAt(2));
            await Navigation.PopAsync();
        }

        async void Handle_Clicked2(object sender, System.EventArgs e)
        {
            await Navigation.PopToRootAsync();
            //Xamarin.Forms.Application.Current.MainPage = new NavigationPage(new Page2());
        }
    }
}

