﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace NavTests
{
    public partial class Page3 : ContentPage
    {
        public Page3()
        {
            InitializeComponent();

            Title = "Page 3";
        }

        async void Handle_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new Page4());
        }
    }
}

