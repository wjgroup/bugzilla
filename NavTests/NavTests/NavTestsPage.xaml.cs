﻿using Xamarin.Forms;

namespace NavTests
{
    public partial class NavTestsPage : ContentPage
    {
        public NavTestsPage()
        {
            InitializeComponent();

            Title = "Page1";
        }

        async void Handle_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new Page2());
        }
    }
}

