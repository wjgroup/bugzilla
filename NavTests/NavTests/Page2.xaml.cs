﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace NavTests
{
    public partial class Page2 : ContentPage
    {
        public Page2()
        {
            InitializeComponent();

            Title = "Page2";
        }

        async void Handle_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new Page3());
        }
    }
}

